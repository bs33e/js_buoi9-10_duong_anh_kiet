
var dsnv = [];
var dsnvSearch = [];
var DSNV = "DSNV";

var dsnvLocalStorage = localStorage.getItem(DSNV);
// console.log('dsnvLocalStorage: ', dsnvLocalStorage);


if (JSON.parse(dsnvLocalStorage)) {
    var data = JSON.parse(dsnvLocalStorage);
    for (var index=0; index < data.length; index++) {
        var current = data[index];
        var nv = new NhanVien (
            current.tknv,
            current.hoVaTen,
            current.email,
            current.matKhau,
            current.ngayThang,
            current.luongCB,
            current.chucVu,
            current.gioLam,      
        );
        dsnv.push(nv);
    }
    renderDSNV(dsnv);
}


function saveLocalStorage() {
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem(DSNV, dsnvJson);

}



function themNguoiDung() {
    var newNV = layThongTinTuForm();
    // Kiểm tra tài khoản nhân viên

    var isValid = validator.kiemTraRong(newNV.tknv, "tbTKNV") && validator.kiemTraDoDai(newNV.tknv, "tbTKNV", 4, 6);
    // && validator.kiemTraTungTk(newNV.tknv, dsnv);
    // Kiểm tra tên nhân viên

    isValid = isValid & validator.kiemTraRong(newNV.hoVaTen, "tbTen") && validator.kiemTraChuoiKiTu(newNV.hoVaTen, "tbTen" );
    // Kiểm tra email

    isValid = isValid & validator.kiemTraRong(newNV.email,"tbEmail") && validator.kiemTraEmail(newNV.email,"tbEmail");

    // Kiểm tra mật khẩu
    isValid = isValid & validator.kiemTraRong(newNV.matKhau, "tbMatKhau") && validator.kiemTraMatKhau(newNV.matKhau, "tbMatKhau");

    // Kiểm tra ngày tháng năm
    isValid = isValid & validator.kiemTraRong(newNV.ngayThang, "tbNgay") && validator.kiemTraNgayThang(newNV.ngayThang, "tbNgay");

    // Kiểm tra lương cơ bản

    isValid = isValid & validator.kiemTraRong(newNV.luongCB, "tbLuongCB") && validator.kiemTraMucLuong(newNV.luongCB, "tbLuongCB");

    // Kiểm tra chức vụ

    isValid = isValid & validator.kiemTraChucVu(newNV.chucVu, "tbChucVu");

    // Kiểm tra giờ làm

    isValid = isValid & validator.kiemTraRong(newNV.gioLam, "tbGiolam") && validator.kiemTraGioLam(newNV.gioLam, "tbGiolam", 80, 200);

    if (isValid == false) {
        return;
    }

    dsnv.push(newNV);
    saveLocalStorage();
    renderDSNV(dsnv);
}



function deleteForm(acc) {
    var index = timKiemViTri(acc, dsnv) ;

    if (index !== -1) {
        dsnv.splice(index, 1);
        saveLocalStorage();

        renderDSNV(dsnv);
    
    }
}

function editForm(acc) {
    var index = timKiemViTri(acc, dsnv) ;
    console.log('index: ', index);
    
    if (index !== -1) {
        var nv = dsnv[index];
        showThongTinLenForm(nv);
    }
}


function capNhatNV() {
    var nvEdit = layThongTinTuForm();
    console.log('nvEdit: ', nvEdit);
    
    var index = timKiemViTri (nvEdit.tknv, dsnv);
    
    if (index !== -1) {

        var isValid = validator.kiemTraRong(nvEdit.tknv, "tbTKNV") && validator.kiemTraDoDai(nvEdit.tknv, "tbTKNV", 4, 6);
        // && validator.kiemTraTungTk(newNV.tknv, dsnv);
        // Kiểm tra tên nhân viên
    
        isValid = isValid & validator.kiemTraRong(nvEdit.hoVaTen, "tbTen") && validator.kiemTraChuoiKiTu(nvEdit.hoVaTen, "tbTen" );
        // Kiểm tra email
    
        isValid = isValid & validator.kiemTraRong(nvEdit.email,"tbEmail") && validator.kiemTraEmail(nvEdit.email,"tbEmail");
    
        // Kiểm tra mật khẩu
        isValid = isValid & validator.kiemTraRong(nvEdit.matKhau, "tbMatKhau") && validator.kiemTraMatKhau(nvEdit.matKhau, "tbMatKhau");
    
        // Kiểm tra ngày tháng năm
        isValid = isValid & validator.kiemTraRong(nvEdit.ngayThang, "tbNgay") && validator.kiemTraNgayThang(nvEdit.ngayThang, "tbNgay");
    
        // Kiểm tra lương cơ bản
    
        isValid = isValid & validator.kiemTraRong(nvEdit.luongCB, "tbLuongCB") && validator.kiemTraMucLuong(nvEdit.luongCB, "tbLuongCB");
    
        // Kiểm tra chức vụ
    
        isValid = isValid & validator.kiemTraChucVu(nvEdit.chucVu, "tbChucVu");
    
        // Kiểm tra giờ làm
    
        isValid = isValid & validator.kiemTraRong(nvEdit.gioLam, "tbGiolam") && validator.kiemTraGioLam(nvEdit.gioLam, "tbGiolam", 80, 200);
    
        if (isValid == false) {
            return;
        }
    

        dsnv[index] = nvEdit;
        saveLocalStorage();
        renderDSNV(dsnv);
    }
}

function hienThiLoaiNhanVien(){
    
    var loaiNV = layThongTinSearch();
    var index = timKiemLoaiNV(loaiNV, dsnv);
    console.log('index: ', index);


}

function hienThiLoaiNhanVien() {

    var loaiNV = layThongTinSearch();
    for(var index=0; index < dsnv.length; index++) {
        var nv = dsnv[index];
        if (nv.xepLoaiNV() == loaiNV) {
            dsnvSearch.push(nv);
            
        }
        renderDSNV(dsnvSearch);
   
    }
    
}