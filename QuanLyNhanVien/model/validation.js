

var validator = {
    kiemTraRong: function (valueIput, tknvError) {
        if(valueIput == "") {
            document.getElementById(tknvError).style.display = "inline";
            document.getElementById(tknvError).innerText = "Không bỏ trống";
            return false;
        }
        else {
            document.getElementById(tknvError).style.display = "none";
            return true;
        }
    }, 
    kiemTraDoDai: function(valueIput, tknvError, min, max) {
        // 4 - 6
        var inputLength = valueIput.length;
        if(inputLength < min || inputLength > max) {
            document.getElementById(tknvError).style.display = "inline";
            document.getElementById(tknvError).innerText = `Độ dài từ ${min} đến ${max} kí tự`;
            return false;
        }
        else {
            document.getElementById(tknvError).style.display = "none";
            return true;
        }
    },
    // kiemTraTungTk: function(tkNV, dsnv) {
    //     var index = List.findIndex((nv) => {
    //         return nv.tknv == tkNV;
    //     });
    //     if (index !== -1) {
    //         document.getElementById("tbTKNV").style.display = "inline";
    //         document.getElementById("tbTKNV").innerText = `Tài khoản đã tồn tại`;
    //         return false;
    //     }
    //     else {
    //         document.getElementById(tknvError).style.display = "none";
    //         return true;
    //     }
    // },
    kiemTraChuoiKiTu: function(valueIput, tenError) {
        var letters = /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
        if (valueIput.match(letters)) {
            return true;
        } 
        else {
            document.getElementById(tenError).style.display = "inline";
            document.getElementById(tenError).innerText = `Tên nhân viên phải là chữ`;
            return false;
        }
    },
    kiemTraEmail: function(valueIput, emailError) {
        var ktEmail = /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/;
        if (valueIput.match(ktEmail)) {
            return true;
        }
        else {
            document.getElementById(emailError).style.display = "inline";
            document.getElementById(emailError).innerText = `Email không hợp lệ`;
            return false; 
        }
    },
    kiemTraMatKhau: function(valueIput, passError) {
        var pass = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,8}$/;
        if (valueIput.match(pass)) {
            return true;
        } 
        else {
            document.getElementById(passError).style.display = "inline";
            document.getElementById(passError).innerText = `Mật khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt`;
            return false;
        }
    },
    kiemTraNgayThang: function(valueIput, dateError) {
        var date = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/ ;
        if (valueIput.match(date)) {
            return true;
        } 
        else {
            document.getElementById(dateError).style.display = "inline";
            document.getElementById(dateError).innerText = `Sai định dạng mm/dd/yyyy`;
            return false;
        }
    },

    kiemTraMucLuong: function(valueIput, luongError) {
        luong = JSON.parse(valueIput)*1;
    
        if (luong >= 1000000 && luong <= 20000000) {
            return true;
        }
        else {
            document.getElementById(luongError).style.display = "inline";
            document.getElementById(luongError).innerText = `Mức lương từ 1000000 - 20000000`;
            return false;
        }
    },
    kiemTraChucVu: function(valueIput, chucVuError) {
        
        if(valueIput == "Chọn chức vụ") {
            document.getElementById(chucVuError).style.display = "inline";
            document.getElementById(chucVuError).innerText = "Chọn lại chức vụ";
            return false;
        }
        else {
            document.getElementById(chucVuError).style.display = "none";
            return true;
        }
    },
    kiemTraGioLam: function (valueIput, gioLamError, min, max) {
        
        if (valueIput >= min && valueIput <= max) {
            return true;
        }
        else {
            document.getElementById(gioLamError).style.display = "inline";
            document.getElementById(gioLamError).innerText = `Số giờ làm từ ${min} - ${max}`;
            return false;
        }
    },
};