
function NhanVien (tknv, hoVaTen, email, matKhau, ngayThang, luongCB, chucVu, gioLam) {
    this.tknv = tknv;
    this.hoVaTen = hoVaTen;
    this.email = email;
    this.matKhau = matKhau;
    this.ngayThang = ngayThang;
    this.luongCB = luongCB;
    this.chucVu = chucVu;
    this.gioLam = gioLam;
    this.tinhTongLuongCB = function() {
        if (this.chucVu == 'Sếp') {
            return this.luongCB *3;
        }
        if (this.chucVu == 'Trưởng phòng'){
            return this.luongCB *2;
        }
        if (this.chucVu == 'Nhân viên'){
            return this.luongCB *1;
        }
        
        
    };
    this.xepLoaiNV = function() {
        if (this.gioLam >= 192) {
            return "Nhân viên xuất sắc";
        }
        else if (this.gioLam >= 176) {
            return "Nhân viên giỏi";

        }
        else if (this.gioLam >= 160) {
            return "Nhân viên khá";

        }
        else {
            return "Nhân viên trung bình";
        }
    };

}
