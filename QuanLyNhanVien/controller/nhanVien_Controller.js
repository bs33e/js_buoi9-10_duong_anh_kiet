

function layThongTinTuForm() {
   
    var tknv = document.getElementById("tknv").value;
    var hoVaTen = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var matKhau = document.getElementById("password").value;
    var ngayThang = document.getElementById("datepicker").value;
    var luongCB = document.getElementById("luongCB").value*1;
    var chucVu = document.getElementById("chucvu").value;
    var gioLam = document.getElementById("gioLam").value*1;

    var nv = new NhanVien (tknv, hoVaTen, email, matKhau, ngayThang, luongCB, chucVu, gioLam);
   
    return nv;
    
}

function layThongTinSearch() {
    var timKiem = document.getElementById("searchName").value;
    return timKiem;
}

function showThongTinLenForm (nv) {

    document.getElementById("tknv").value = nv.tknv;
    document.getElementById("name").value = nv.hoVaTen;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.matKhau;
    document.getElementById("datepicker").value = nv.ngayThang;
    document.getElementById("luongCB").value = nv.luongCB;
    document.getElementById("chucvu").value = nv.chucVu;
    document.getElementById("gioLam").value = nv.gioLam;

}



function renderDSNV(list) {
    var contentHTML = "";
    list.forEach(function(item) {
        var content = `<tr>
        <td>${item.tknv}</td>
        <td>${item.hoVaTen}</td>
        <td>${item.email}</td>
        <td>${item.ngayThang}</td>
        <td>${item.chucVu}</td>
        <td>${item.tinhTongLuongCB()}</td>
        <td>${item.xepLoaiNV()}</td>
        <td>${item.gioLam}</td>
        <td>
        <button onclick="deleteForm(${item.tknv})" type="button" name="" id="" class="btn btn-danger" btn-lg btn-block"><i class="fa-solid fa-trash-can"></i></button>
        <button onclick="editForm(${item.tknv})" data-toggle="modal" data-target="#myModal" type="button" name="" id="" class="btn btn-warning" btn-lg btn-block"><i class="fa-solid fa-pen-to-square"></i></button>
        </td>
        </tr>`;
        contentHTML += content;
    });
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}


function timKiemViTri(acc, arr) {
    for(var index=0; index < arr.length; index++) {
        var nv = arr[index];
        if (nv.tknv == acc) {
            return index;
        }

    }
    return -1;
}

function timKiemLoaiNV(acc, arr) {
    for(var index=0; index < arr.length; index++) {
        var nv = arr[index];
        if (nv.xepLoaiNV() == acc) {
            return index;
        }

    }
    return -1;
}